import { Meteor } from 'meteor/meteor';

import '../Model/collections.js'
import { Units } from '../Model/collections.js'
import { Attacks } from '../Model/collections.js'
import { Abilities } from '../Model/collections.js'
import { Wargear } from '../Model/collections.js'
import { Allegiances } from '../Model/collections.js'
import { Allegiance_Abilities } from '../Model/collections.js'
import { Command_Traits } from '../Model/collections.js'
import { Artifacts } from '../Model/collections.js'
import { Endless_Spells } from '../Model/collections.js'
import { Spells } from '../Model/collections.js';
import { Battalions } from '../Model/collections.js';
Meteor.startup(() => {
  if(typeof require !== 'undefined'){
    XLSX = require('xlsx');
    fs = require('fs');
  }

  // var files = getxlsxFilesInPublicFolder();
  // for(var index = 0; index < files.length; index++){
  //   var workbook = XLSX.readFile(process.env.PWD + '/public/units/'+files[index]);
  //   parseWorkbook(workbook);
  // }
});

Meteor.publish("spells", function(){
  var spells = Spells.find();
  return spells;
})

Meteor.publish("battalions", function(allegiance){
  allegiance = allegiance.toUpperCase();
  var battalions = Battalions.find({allegiance: allegiance});
  return battalions
})

Meteor.publish("endlessSpells", function(){
  var spells = Endless_Spells.find();
  return spells
})

Meteor.publish("allegiances", function (id) {
  return Allegiances.find();
});

Meteor.publish("units", function (allegiance) {
  var self=this;

  if(allegiance == 'Legion of Sacrament'){
    allegiance = "LEGIONS OF NAGASH"
  }
  else if(allegiance == 'Legion of Night'){
    allegiance = "LEGIONS OF NAGASH"
  }
  else if(allegiance == 'Legion of Blood'){
    allegiance = "LEGIONS OF NAGASH"
  }
  else if(allegiance == 'Grand Host of Nagash'){
    allegiance = "LEGIONS OF NAGASH"
  }

  var units = Units.find({allegiance: allegiance.toUpperCase()});
  return units;
});

Meteor.publish("allegianceAbilities", function(allegiance){
  console.log(allegiance)
  return [
    Allegiance_Abilities.find({allegiance: allegiance.toUpperCase()}),
    Artifacts.find({allegiance: allegiance}),
    Command_Traits.find({allegiance: allegiance})
  ];
})

Meteor.publish("realmAllegianceAbilities", function(realm){
  if(realm == 'Death'){
    realm = 'Shyish';
  }
  return [
    Allegiance_Abilities.find({allegiance: realm}),
    Artifacts.find({allegiance: realm}),
    Command_Traits.find({allegiance: realm})
  ];
})

Meteor.methods({
  upsertUnit: function(unit){
    Units.update({_id: unit._id}, unit, {upsert: true});
  },
  processAbilities: function(){
    var units = Units.find().fetch();
    for(var index = 0; index < units.length; index++){
      var unit = units[index];
      unit.costPerUnit = parseInt(unit.costPerUnit, 10);
      Units.update({_id: unit._id}, unit);
    }
  },
  getSeraphonSummonTableText: function(){
    return Allegiance_Abilities.findOne({_id: '4lnEKB8bOvHffyGjR'});
  },
  getSeraphonSummonTable: function(){
    return Allegiance_Abilities.findOne({_id: 'kQBt50pzq7H7vKwJG'});
  },
  getKhorneTitheTableText: function(){
    return Allegiance_Abilities.findOne({_id: 'aVkVsaToBGUDoJ'});
  },
  getKhorneRewardsTable: function(){
    return Allegiance_Abilities.findOne({_id: 'XNfKysYaPaTNhR'});
  },
  getKhorneSummonTableText: function(){
    return Allegiance_Abilities.findOne({_id: 'svAuqdPWjtGdLv'});
  },
  getKhorneSummonTable: function(){
    return Allegiance_Abilities.findOne({_id: 'bIIQFTQmiJXdgN'});
  },
  getNurgleSummonTableText: function(){
    return Allegiance_Abilities.findOne({_id: 'weMjPoK9pdF6QUVCN'});
  },
  getNurgleSummonTable: function(){
    return Allegiance_Abilities.findOne({_id: 'g0DMz4nGrCW8Bo5d5'});
  },
  getNurgleCorruptionTableText: function(){
    return Allegiance_Abilities.findOne({_id: 'V5sdiOFbF8kOamiQi'});
  },
  getNurgleCorruptionTable: function(){
    return Allegiance_Abilities.findOne({_id: 'hQ6e9FDpc6mz5Hg7u'});
  },
  getGardensOfNurgle: function(){
    return Allegiance_Abilities.findOne({_id: 'dNYrPfOu1YBKSToOF'});
  }
})

getxlsxFilesInPublicFolder = function() {
  var _ = require('underscore');
  var files = fs.readdirSync(process.env.PWD + '/public/units');

  var cleanedUpFiles = _(files).reject( function(fileName) {
    return fileName.indexOf('.xlsx') < 0;
  });

  return cleanedUpFiles;
}

parseAllegiance = function(){
  var _ = require('underscore');
  var workbook = XLSX.readFile(process.env.PWD + '/public/Allegiances.xlsx');

  var chaosSheet = workbook.Sheets["Chaos"];
  if(chaosSheet != undefined){
    parseAllegianceSheet(chaosSheet, "Chaos");
  }

  var deathSheet = workbook.Sheets["Death"];
  if(deathSheet != undefined){
    parseAllegianceSheet(deathSheet, "Death");
  }

  var destructionSheet = workbook.Sheets["Destruction"];
  if(destructionSheet != undefined){
    parseAllegianceSheet(destructionSheet, "Destruction");
  }

  var orderSheet = workbook.Sheets["Order"];
  if(orderSheet != undefined){
    parseAllegianceSheet(orderSheet, "Order");
  }
}
parseAllegianceSheet = function(sheet, alliance){
  var range = XLSX.utils.decode_range(sheet['!ref']);
  var keys = getSheetHeader(sheet);

  for(rowNum=range.s.r; rowNum<=range.e.r; rowNum++){
    var currentCell = sheet[
      XLSX.utils.encode_cell({r: rowNum, c: 0})
    ];
    if( typeof currentCell !== 'undefined' && currentCell !== ''){
      var obj = {
        name: currentCell.w,
        alliance: alliance
      }
      Allegiances.update({name: obj.name},obj,{upsert: true});
    }
  }
}
parseWorkbook = function(workbook){
  XLSX = require('xlsx');
  _ = require('underscore');

  var baseSheet = workbook.Sheets["Base"];
  if(baseSheet != undefined){
    parseBaseSheet(baseSheet);
    var attackSheet = workbook.Sheets["Attacks"];
    parseAttackSheet(attackSheet);
    var abilitySheet = workbook.Sheets["Abilities"];
    parseAbilitySheet(abilitySheet)
    // var wargearSheet = workbook.Sheets["Wargear"];
    // parseWargearSheet(wargearSheet)
  }
}
getSheetHeader = function(sheet){
  var keys = [];
  var range = XLSX.utils.decode_range(sheet['!ref']);
  for(colNum=range.s.c; colNum<=range.e.c; colNum++){
    var nextCell = sheet[
      XLSX.utils.encode_cell({r: 0, c: colNum})
    ];
    if( typeof nextCell !== 'undefined' ){
      keys.push(nextCell.w);
    }
  }

  return keys;
}
parseBaseSheet = function(baseSheet){
  //establish the json for Units doc by looking at header row
  var range = XLSX.utils.decode_range(baseSheet['!ref']);
  var keys = getSheetHeader(baseSheet);

  for(rowNum=range.s.r+1; rowNum<=range.e.r; rowNum++){
    var doc = new Object();
    for(colNum=range.s.c; colNum<=range.e.c; colNum++){
      var currentCell = baseSheet[
        XLSX.utils.encode_cell({r: rowNum, c: colNum})
      ];
      if( typeof currentCell !== 'undefined' && currentCell !== ''){
        if(keys[colNum] == 'keywords'){
          doc[keys[colNum]] = [];
          var keywords = currentCell.w.split(',');
          for(index=0;index<keywords.length;index++){
            var keyword = keywords[index].trim();
            doc[keys[colNum]].push(keyword);
          }
        }
        else{
          doc[keys[colNum]] = currentCell.w;
        }
      }
    }
    if(JSON.stringify(doc) !== JSON.stringify({})){
      doc['attacks'] = [];
      Units.update({name: doc.name, allegiance: doc.allegiance}, {$set:doc}, {upsert: true});
    }
  }
}
parseAttackSheet = function(attackSheet){
  if(attackSheet === undefined){
    return;
  }
  //get the header
  var range = XLSX.utils.decode_range(attackSheet['!ref']);
  var keys = getSheetHeader(attackSheet);

  for(rowNum=range.s.r; rowNum<=range.e.r; rowNum++){
    var unitName = attackSheet[
      XLSX.utils.encode_cell({r: rowNum, c: 0})
    ];
    var unit = undefined;
    if( typeof unitName !== 'undefined'){
      unit = Units.findOne({name: unitName.w});
    }
    console.log(unit)
    if(unit != undefined){
      var doc = new Object();
      for(colNum=range.s.c + 1; colNum<=range.e.c; colNum++){
        var currentCell = attackSheet[
          XLSX.utils.encode_cell({r: rowNum, c: colNum})
        ];
        if( typeof currentCell !== 'undefined'){
          doc[keys[colNum]] = currentCell.w
        }
      }
      if(JSON.stringify(doc) !== JSON.stringify({})){
        var attack = Attacks.findOne({name: doc.name, type: doc.type, range: doc.range, numAttacks: doc.numAttacks});
        var attackId = undefined;

        // if(attack != undefined){
        //   attackId = attack._id;
        //   Attacks.update({name: doc.name, type: doc.type, range: doc.range, numAttacks: doc.numAttacks}, {$set:doc}, {upsert: true});
        // }
        // else {
        //   attackId = Attacks.insert(doc);
        // }

        Units.update({_id: unit._id}, {$addToSet:{attacks: doc}});
      }
    }
  }
}
parseAbilitySheet = function(abilitySheet){
  if(abilitySheet === undefined){
    return;
  }
  //get the header
  var range = XLSX.utils.decode_range(abilitySheet['!ref']);
  var keys = getSheetHeader(abilitySheet);

  for(rowNum=range.s.r+1; rowNum<=range.e.r; rowNum++){
    var unitName = abilitySheet[
      XLSX.utils.encode_cell({r: rowNum, c: 0})
    ];
    var unit = undefined;
    if( typeof unitName !== 'undefined'){
      unit = Units.findOne({name: unitName.w});
    }

    if(unit != undefined){
      var doc = new Object();
      for(colNum=range.s.c + 1; colNum<=range.e.c; colNum++){
        var currentCell = abilitySheet[
          XLSX.utils.encode_cell({r: rowNum, c: colNum})
        ];
        if( typeof currentCell !== 'undefined'){
          if(keys[colNum] == 'phase'){
            doc[keys[colNum]] = [];

            var keywords = currentCell.w.split(',');
            for(index=0;index<keywords.length;index++){
              var keyword = keywords[index].trim();
              doc[keys[colNum]].push(keyword);
            }
          }
          else{
            doc[keys[colNum]] = currentCell.w;
          }
        }
      }

      if(JSON.stringify(doc) !== JSON.stringify({})){
        var ability = Abilities.findOne({name: doc.name, text: doc.text});
        var abilityId;

        // if(ability != undefined){
        //   abilityId = ability._id;
        //   Abilities.update({_id: abilityId}, {$set:doc}, {upsert: true});
        // }
        // else {
        //   abilityId = Abilities.insert(doc);
        // }

        Units.update({_id: unit._id}, {$addToSet:{abilities: doc}});
      }
    }
  }
}
parseWargearSheet = function(sheet){
  if(sheet === undefined){
    return;
  }
  //get the header
  var range = XLSX.utils.decode_range(sheet['!ref']);
  var keys = getSheetHeader(sheet);

  for(rowNum=range.s.r+1; rowNum<=range.e.r; rowNum++){
    var unitName = sheet[
      XLSX.utils.encode_cell({r: rowNum, c: 0})
    ];
    var unit = undefined;
    if( typeof unitName !== 'undefined'){
      unit = Units.findOne({name: unitName.w});
    }

    if(unit != undefined){
      var doc = new Object();
      for(colNum=range.s.c + 1; colNum<=range.e.c; colNum++){
        var currentCell = sheet[
          XLSX.utils.encode_cell({r: rowNum, c: colNum})
        ];
        if( typeof currentCell !== 'undefined'){
          doc[keys[colNum]] = currentCell.w
        }
      }

      if(JSON.stringify(doc) !== JSON.stringify({})){
        var ability = Wargear.findOne({name: doc.name, text: doc.text});
        var abilityId = undefined;

        if(ability != undefined){
          abilityId = ability._id;
          Wargear.update({_id: abilityId}, {$set:doc}, {upsert: true});
        }
        else {
          abilityId = Wargear.insert(doc);
        }

        Units.update({_id: unit._id}, {$addToSet:{wargear: abilityId}});
      }
    }
  }
}

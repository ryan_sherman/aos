checkRanges = function(attack, toHit, toWound, damage, rend, reRollHits, reRollWounds, explodingHits, explodingWounds, mortalWounds) {
  //check to see if any of the values are a range of values
  if (attack.typeof == String && attack.indexOf('-') > -1) {
    var attackArray = attack.split('-');
    var attackMin = parseInt(attackArray[0]);
    var attackMax = parseInt(attackArray[1]);

    var minDamage = damageFormula(attackMin, toHit, toWound, damage);
    var maxDamage = damageFormula(attackMax, toHit, toWound, damage);

    if (isNaN(minDamage) || isNaN(maxDamage)) {
      return "*";
    }

    return minDamage + ' to ' + maxDamage;
  } else if (toHit.typeof == String && toHit.indexOf('-') > -1) {
    var toHitArray = toHit.split('-');
    var toHitMin = parseInt(toHitArray[1]);
    var toHitMax = parseInt(toHitArray[0]);

    var minDamage = damageFormula(attack, toHitMin, toWound, damage);
    var maxDamage = damageFormula(attack, toHitMax, toWound, damage);

    if (isNaN(minDamage) || isNaN(maxDamage)) {
      return "*";
    }

    return minDamage + ' to ' + maxDamage;
  } else if (toWound.typeof == String && toWound.indexOf('-') > -1) {
    var toWoundArray = toWound.split('-');
    var toWoundMin = parseInt(toWoundArray[1]);
    var toWoundMax = parseInt(toWoundArray[0]);

    var minDamage = damageFormula(attack, toHit, toWoundMin, damage);
    var maxDamage = damageFormula(attack, toHit, toWoundMax, damage);

    if (isNaN(minDamage) || isNaN(maxDamage)) {
      return "*";
    }

    return minDamage + ' to ' + maxDamage;
  } else if (damage.typeof == String && damage.indexOf('-') > -1) {
    var damageArray = damage.split('-');
    var damageMin = parseInt(damageArray[0]);
    var damageMax = parseInt(damageArray[1]);

    var minDamage = damageFormula(attack, toHit, toWound, damageMin);
    var maxDamage = damageFormula(attack, toHit, toWound, damageMax);

    if (isNaN(minDamage) || isNaN(maxDamage)) {
      return "*";
    }

    return minDamage + ' to ' + maxDamage;
  } else {
    var damage = damageFormula(attack, toHit, toWound, damage, reRollHits, reRollWounds, explodingHits, explodingWounds, mortalWounds);
    if (isNaN(damage)) {
      return "*";
    }
    console.log(damage)
    damage = calcRend(damage, rend);
    return Math.round(damage * 100) / 100;
  }
}
calcRend = function(damage, rend) {
  var baseSave = 3;
  var rend = parseInt(rend);
  if (isNaN(rend)) {
    rend = 0;
  }
  return damage * ((baseSave - rend) / 6);
}
damageFormula = function(attack, toHit, toWound, damage, reRollHits, reRollWounds, explodingHits, explodingWounds, mortalWounds) {
  attack = convertDiceOrParse(attack);
  toHit = convertDiceOrParse(toHit);
  toWound = convertDiceOrParse(toWound);
  damage = convertDiceOrParse(damage);

  if (toHit < 2) {
    toHit = 2;
  }
  if (toWound < 2) {
    toWound = 2;
  }

  var probToHit = ((((toHit - 1) - 6) * (-1)) / 6);
  if (reRollHits != undefined) {
    probToHit = reRolls(probToHit, reRollHits);
  }
  if (explodingHits != undefined) {
    probToHit = explodingDice(probToHit, explodingHits, reRollHits)
  }

  var probToWound = ((((toWound - 1) - 6) * (-1)) / 6);
  if (reRollWounds != undefined) {
    probToWound = reRolls(probToWound, reRollWounds);
  }
  if (explodingWounds != undefined) {
    probToWound = explodingDice(probToWound, explodingWounds, reRollWounds)
  }

  var damageVal = attack * probToHit * probToWound * damage;
  if (mortalWounds !== undefined) {
    damageVal += probToHit * ((((mortalWounds - 1) - 6) * (-1)) / 6);
  }
  return Math.round(damageVal * 100) / 100;
}
reRolls = function(probPass, reRoll) {
  //Probability of pass + ( 1/6 * Probability of Pass)
  var probReroll = reRoll / 6;
  return probPass + (probReroll * probPass);
}
explodingDice = function(probPass, explodingValue, reRoll) {
  //Probability of explode * D6 + Probability of Pass - Probability of explode
  var probExplode = ((((explodingValue - 1) - 6) * (-1)) / 6);
  if (reRoll != undefined) {
    var probReroll = reRoll / 6;
    probExplode = probExplode + (probReroll * probExplode);
  }
  var averageD6 = 3.5;
  return (probExplode * averageD6) + (probPass - probExplode);
}
convertDiceOrParse = function(value) {
  try {
    if (value.indexOf('D') > -1) {
      var values = value.split('D');

      var diceNum = parseInt(values[1]);
      var returnValue = 0;
      for (var x = 0; x <= diceNum; x++) {
        returnValue = returnValue + x;
      }

      var returnNum = returnValue / diceNum;
      if (values[0] != "") {
        returnNum = returnNum * values[0];
      }
      return returnNum;
    }
  } catch (ex) {}
  return parseInt(value);
}

randomCharacters = function(numOfChars) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < numOfChars; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
Array.prototype.contains = function(obj) {
  var i = this.length;
  while (i--) {
    if (this[i] == obj) {
      return true;
    }
  }
  return false;
}
Array.prototype.compareArray = function(array) {
  for (var i = 0, l = this.length; i < l; i++) {
    var unitPhase = this[i];
    if (array.contains(unitPhase)) {
      return false;
    }
  }
  return true;
}
customSort = function(a, b) {
  if (a.unitName < b.unitName)
    return -1;
  if (a.unitName > b.unitName)
    return 1;
  if (a.when == 'Start' && b.when != 'Start') {
    return -1;
  } else if (a.when == 'Start' && b.when != 'Start') {
    return 1;
  } else if (a.when == 'During' && b.when == 'End') {
    return -1;
  } else if (a.when == 'During' && b.when == 'End') {
    return 1;
  }
  return 0;
}
sortWhen = function(array, field) {
  return array.sort(function(a, b) {
    return a.unitName - b.unitName;
    a_Field = a[field];
    b_Field = b[field];

    if (a_Field == 'Start' && b_Field != 'Start') {
      return -1;
    } else if (b_Field == 'Start' && a_Field != 'Start') {
      return 1;
    } else if (a_Field == 'During' && b_Field == 'End') {
      return -1;
    } else if (b_Field == 'During' && a_Field == 'End') {
      return 1;
    }
    return a.unitName - b.unitName;
  });
}
Handlebars.registerHelper('trim', function(string) {
  if (string != undefined) {
    return string.trim();
  }
  return string;
});
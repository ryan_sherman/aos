import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Allegiance_Abilities } from '../../Model/collections.js';

Template.nurgleSpecial.onCreated(function(){
  // counter starts at 0
  var self = this;
  this.summonText = new ReactiveVar([]);
  this.summonTable = new ReactiveVar([]);
  this.corruptionText = new ReactiveVar([]);
  this.corruptionTable = new ReactiveVar([]);
  this.gardensOfNurgle = new ReactiveVar([]);
  Meteor.call("getNurgleSummonTableText", function(error, result){
    if(!error){
      if(result != undefined){
        self.summonText.set(result);
      }
    }
  });
  Meteor.call("getNurgleSummonTable", function(error, result){
    if(!error){
      if(result != undefined){
        self.summonTable.set(result);
      }
    }
  });
  Meteor.call("getNurgleCorruptionTableText", function(error, result){
    if(!error){
      if(result != undefined){
        self.corruptionText.set(result);
      }
    }
  });
  Meteor.call("getNurgleCorruptionTable", function(error, result){
    if(!error){
      if(result != undefined){
        self.corruptionTable.set(result);
      }
    }
  });
  Meteor.call("getGardensOfNurgle", function(error, result){
    if(!error){
      if(result != undefined){
        self.gardensOfNurgle.set(result);
      }
    }
  });
});

Template.nurgleSpecial.helpers({
  summonName: function(){
    var ability = Template.instance().summonText.get();
    if(ability != undefined && ability.name != undefined){
      return ability.name;
    }
  },
  summonText: function(arrayIndex){
    var ability = Template.instance().summonText.get();
    if(ability != undefined && ability.text != undefined){
      return ability.text;
    }
  },
  summonTableName: function(){
    var ability = Template.instance().summonTable.get();
    if(ability != undefined && ability.name != undefined){
      return ability.name;
    }
  },
  summonTableEntry: function(){
    var ability = Template.instance().summonTable.get();
    if(ability != undefined && ability.rewards != undefined){
      return ability.rewards;
    }
  },
  corruptionTextName: function(){
    var ability = Template.instance().corruptionText.get();
    if(ability != undefined && ability.name != undefined){
      return ability.name;
    }
  },
  corruptionText: function(){
    var ability = Template.instance().corruptionText.get();
    if(ability != undefined && ability.text != undefined){
      return ability.text;
    }
  },
  corruptionTableName: function(){
    var ability = Template.instance().corruptionTable.get();
    if(ability != undefined && ability.name != undefined){
      return ability.name;
    }
  },
  corruptionTableEntry: function(){
    var ability = Template.instance().corruptionTable.get();
    if(ability != undefined && ability.rewards != undefined){
      return ability.rewards;
    }
  },
  gardensName: function(){
    var ability = Template.instance().gardensOfNurgle.get();
    if(ability != undefined && ability.name != undefined){
      return ability.name;
    }
  }
});

Template.nurgleSpecial.events({

});

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Allegiance_Abilities } from '../../Model/collections.js';

Template.khorneTitheTable.onCreated(function(){
  // counter starts at 0
  var self = this;
  this.titheAbility = new ReactiveVar([]);
  this.rewardsTable = new ReactiveVar([]);
  this.summonAbility = new ReactiveVar([]);
  this.summonsTable = new ReactiveVar([]);
  Meteor.call("getKhorneTitheTableText", function(error, result){
    if(!error){
      if(result != undefined){}
      self.titheAbility.set(result);
    }
  });
  Meteor.call("getKhorneRewardsTable", function(error, result){
    if(!error){
      if(result != undefined){}
      self.rewardsTable.set(result);
    }
  });
  Meteor.call("getKhorneSummonTableText", function(error, result){
    if(!error){
      if(result != undefined){}
      self.summonAbility.set(result);
    }
  });
  Meteor.call("getKhorneSummonTable", function(error, result){
    if(!error){
      if(result != undefined){}
      self.summonsTable.set(result);
    }
  });
});

Template.khorneTitheTable.helpers({
  titheName: function(){
    var ability = Template.instance().titheAbility.get();
    if(ability != undefined && ability.name != undefined){
      return ability.name;
    }
  },
  titheText: function(){
    var ability = Template.instance().titheAbility.get();
    if(ability != undefined && ability.text != undefined){
      return ability.text;
    }
  },
  summonName: function(){
    var ability = Template.instance().summonAbility.get();
    if(ability != undefined && ability.name != undefined){
      return ability.name;
    }
  },
  summonText: function(arrayIndex){
    var ability = Template.instance().summonAbility.get();
    if(ability != undefined && ability.text != undefined){
      return ability.text;
    }
  },
  titheTableEntry: function(){
    var ability = Template.instance().rewardsTable.get();
    if(ability != undefined && ability.rewards != undefined){
      return ability.rewards
    }
  },
  summonTableEntry: function(){
    var ability = Template.instance().summonsTable.get();
    if(ability != undefined && ability.rewards != undefined){
      return ability.rewards;
    }
  },
});

Template.khorneTitheTable.events({

});

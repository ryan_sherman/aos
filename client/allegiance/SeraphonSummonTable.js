import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Allegiance_Abilities } from '../../Model/collections.js';

Template.seraphonTable.onCreated(function(){
  // counter starts at 0
  var self = this;
  this.summonAbility = new ReactiveVar([]);
  this.summonsTable = new ReactiveVar([]);

  Meteor.call("getSeraphonSummonTableText", function(error, result){
    if(!error){
      if(result != undefined){}
      self.summonAbility.set(result);
    }
  });
  Meteor.call("getSeraphonSummonTable", function(error, result){
    if(!error){
      if(result != undefined){}
      self.summonsTable.set(result);
    }
  });
});

Template.seraphonTable.helpers({
  summonName: function(){
    var ability = Template.instance().summonAbility.get();
    if(ability != undefined && ability.name != undefined){
      return ability.name;
    }
  },
  summonText: function(arrayIndex){
    var ability = Template.instance().summonAbility.get();
    if(ability != undefined && ability.text != undefined){
      return ability.text;
    }
  },
  summonTableEntry: function(){
    var ability = Template.instance().summonsTable.get();
    if(ability != undefined && ability.rewards != undefined){
      return ability.rewards;
    }
  },
});

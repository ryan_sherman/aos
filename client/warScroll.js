import '../global/globalClient.js'
import '../global/jQuery-sortable.js'

Template.warScroll.helpers({
  averageWounds: function(attack) {
    var averageWounds = checkRanges(attack.numAttacks, attack.toHit, attack.toWound, attack.damage, attack.rend);
    return averageWounds;
  },
  keywords: function(keywords) {
    var string = "";
    for (var index = 0; index < keywords.length; index++) {
      var keyword = keywords[index];
      string += keyword
      if (index != keywords.length - 1) {
        string += ', '
      }
    }
    return string;
  },
  // abilities: function(abilities){
  //
  // }
})
Template.warScroll.events({
  'click #print': function() {
    var elem = document.getElementById(this.name);
    console.log(this)
    var domClone = elem.cloneNode(true);

    var $printSection = document.getElementById("printSection");

    if (!$printSection) {
      var $printSection = document.createElement("div");
      $printSection.id = "printSection";
      document.body.appendChild($printSection);
    }

    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    document.title = this.name;
    window.print();
  }
})
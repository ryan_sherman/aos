Template.damageCalc.events({
  'click #calcDamage': function(event) {

    var attack = JSON.parse(JSON.stringify(Template.instance().data));
    var plusAttacks = parseInt($('#plusAttacks').val());
    plusAttacks = !isNaN(plusAttacks) ? plusAttacks : 0;
    var plusHit = parseInt($('#plusHit').val());
    plusHit = !isNaN(plusHit) ? plusHit : 0;
    var plusWound = parseInt($('#plusWound').val());
    plusWound = !isNaN(plusWound) ? plusWound : 0;
    var plusRend = parseInt($('#plusRend').val());
    plusRend = !isNaN(plusRend) ? plusRend : 0;
    var plusDamage = parseInt($('#plusDamage').val());
    console.log(plusDamage);
    if (isNaN(plusDamage)) {
      plusDamage = convertDiceOrParse($('#plusDamage').val());
      if (isNaN(plusDamage)) {
        plusDamage = 0;
      }
    }

    var rrHits = parseInt($('#rrHits').val());
    rrHits = !isNaN(rrHits) ? rrHits : undefined;
    var rrWounds = parseInt($('#rrWounds').val());
    rrWounds = !isNaN(rrWounds) ? rrWounds : undefined;
    var explodingHitsPlus = parseInt($('#explodingHits').val());
    explodingHitsPlus = !isNaN(explodingHitsPlus) ? explodingHitsPlus : undefined;
    var explodingWoundsPlus = parseInt($('#explodingWounds').val());
    explodingWoundsPlus = !isNaN(explodingWoundsPlus) ? explodingWoundsPlus : undefined;
    var mortalWoundsPlus = parseInt($('#mortalWoundsPlus').val());
    var mortalWounds = parseInt($('#mortalWounds').val());
    mortalWounds = !isNaN(mortalWounds) ? mortalWounds : undefined;
    var mortalWoundsPlusOnly = parseInt($('#mortalWoundsPlusOnly').val());

    if (isNaN(attack.numAttacks)) {
      attack.numAttacks = convertDiceOrParse(attack.numAttacks);
    } else {
      attack.numAttacks = parseInt(attack.numAttacks);
    }
    attack.numAttacks = attack.numAttacks + plusAttacks;
    attack.toHit = parseInt(attack.toHit);
    attack.toHit = attack.toHit - plusHit;
    attack.toWound = parseInt(attack.toWound);
    attack.toWound = attack.toWound - plusWound;
    attack.rend = parseInt(attack.rend);
    attack.rend = attack.rend + plusRend;
    if (isNaN(attack.damage)) {
      attack.damage = convertDiceOrParse(attack.damage);
    } else {
      attack.damage = parseInt(attack.damage);
    }
    attack.damage = attack.damage + plusDamage;
    console.log(attack.damage)
    var damage = checkRanges(attack.numAttacks, attack.toHit, attack.toWound, attack.damage, attack.rend, rrHits, rrWounds, explodingHitsPlus, explodingWoundsPlus, mortalWounds);

    $('#damageOutput').html(damage + " wounds dealt");
  }
})
Template.damageCalc.helpers({
  averageWounds: function() {
    console.log(Template.instance().data);
    var attack = Template.instance().data;
    var averageWounds = checkRanges(attack.numAttacks, attack.toHit, attack.toWound, attack.damage, attack.rend);
    return averageWounds;
  }
})
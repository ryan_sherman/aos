import {
  Template
} from 'meteor/templating';
import {
  ReactiveVar
} from 'meteor/reactive-var';
import {
  Modal
} from 'meteor/peppelg:bootstrap-3-modal'
import {
  Allegiances
} from '../Model/collections.js';
import {
  Units
} from '../Model/collections.js';
import '../global/globalClient.js'
import './main.html';
import './newUnit.html';
import './newUnit.js';
import './warScroll.html';
import './warScroll.js';
import './damageCalc.html';
import './damageCalc.js';
import './armyBuilder.html';
import './armyBuilder.js';
import './allegiance/khorneTitheTable.html';
import './allegiance/khorneTitheTable.js';
import './allegiance/nurgleSpecial.html';
import './allegiance/nurgleSpecial.js';
import './allegiance/SeraphonSummonTable.html';

Template.header.onCreated(function() {
  // counter starts at 0
  var defaultSelected = 'Khorne';
  this.alliance = new ReactiveVar('Chaos');
  this.armyAlliegance = new ReactiveVar(defaultSelected);
  this.unitAlliegance = new ReactiveVar(defaultSelected);
  Session.set('allegiance', defaultSelected);
  Meteor.subscribe("allegiances");
});

Template.header.helpers({
  allianceSelected: function(option) {
    var alliance = Template.instance().alliance.get();
    if (option === alliance) {
      return "selected";
    }
  },
  armyAllieganceSelected: function() {
    if (this.name === Session.get("allegiance")) {
      return "selected";
    }
  },
  unitAllieganceSelected: function() {
    if (this.name === Template.instance().unitAlliegance.get()) {
      return "selected";
    }
  },
  allegiance: function() {
    var alliance = Template.instance().alliance.get();
    return Allegiances.find({
      alliance: alliance
    }).fetch();
  },
  leaders: function() {
    var alliance = Template.instance().alliance.get();
  },
  unitAlliegance: function() {
    var unitAlliegance = Template.instance().unitAlliegance.get();
    Meteor.subscribe("units", unitAlliegance);
    console.log(Units.find().fetch());
    Meteor.subscribe("battalions", unitAlliegance);
    return unitAlliegance;
  },
  extra: function() {
    var alliegance = Template.instance().armyAlliegance.get();
    Meteor.subscribe("allegianceAbilities", alliegance);
    switch (alliegance) {
      case 'Khorne':
        return 'khorneTitheTable';
      case 'Nurgle':
        return 'nurgleSpecial';
      case 'Seraphon':
        return 'seraphonTable';
    }
  }
});

Template.header.events({
  'click #newUnit': function() {
    Modal.show('newUnitModal', {});
  },
  'change #alliance': function() {
    var alliance = $('#alliance').find(":selected").text();
    Template.instance().alliance.set(alliance);
  },
  'change #armyAlliegance': function() {
    var armyAlliegance = $('#armyAlliegance').find(":selected").text();
    Session.set('allegiance', armyAlliegance);
    Template.instance().armyAlliegance.set(armyAlliegance);
  },
  'change #unitAlliegance': function() {
    var unitAlliegance = $('#unitAlliegance').find(":selected").text();
    Template.instance().unitAlliegance.set(unitAlliegance);
  },
});

reverseArray = function(oldArray) {
  var newArray = [];
  for (var index = oldArray.length - 1; index >= 0; index--) {
    newArray.push(oldArray[index]);
  }
  console.log(newArray)
}
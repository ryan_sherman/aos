import '../global/globalClient.js'
import '../global/jQuery-sortable.js'
Template.newUnitModal.onCreated(function() {
  var keywords = (this.data != undefined && this.data.keywords == undefined ? [] : this.data.keywords);
  this.keywords = new ReactiveVar(keywords);
  this.selectedAttack = new ReactiveVar();
  this.selectedAbility = new ReactiveVar();

  var attacks = this.data.attacks;
  if (attacks == undefined) {
    attacks = [];
  } else {
    for (var index = 0; index < attacks.length; index++) {
      var attack = attacks[index];
      attack._id = (attack._id == undefined ? randomCharacters(9) : attack._id);
      var averageWounds = checkRanges(attack.numAttacks, attack.toHit, attack.toWound, attack.damage, attack.rend);
      var attackHtml = '<td>' + attack.name + '</td><td>' + attack.type + '</td><td>' + attack.range + '</td><td>' + attack.numAttacks + '</td><td>' + attack.toHit + '</td><td>' + attack.toWound + '</td><td>' + attack.rend + '</td><td>' + attack.damage + '</td><td>' + averageWounds + '</td>' +
        '<td>' +
        '<i id=\"' + attack._id + '\" class=\"deleteAttack glyphicon glyphicon-remove\" style=\"float: right; color: red;\"></i>' +
        '<i id=\"' + attack._id + '\" class=\"upAttack glyphicon glyphicon-arrow-up\" style=\"float: right; color: black;\"></i>' +
        '<i id=\"' + attack._id + '\" class=\"downAttack glyphicon glyphicon-arrow-down\" style=\"float: right; color: black;\"></i>' +
        '</td>';
      attack['html'] = attackHtml;
    }
  }
  this.attacks = new ReactiveVar(attacks);

  var abilities = this.data.abilities;
  if (abilities == undefined) {
    abilities = [];
  } else {
    for (var index = 0; index < abilities.length; index++) {
      var ability = abilities[index];
      ability._id = (ability._id == undefined ? randomCharacters(9) : ability._id);
      var abilityHtml =
        '<div class="col-lg-3">' +
        '<strong>' + ability.name + '</strong> (' + ability.type + '):' +
        '</div>' +
        '<div style="overflow-x: auto" class="col-lg-5">' +
        ability.text +
        '</div>' +
        '<div class="col-lg-2">' +
        ability.phase +
        '</div>' +
        '<div class="col-lg-2">' +
        '<i id=\"' + ability._id + '\" class=\"deleteAbility glyphicon glyphicon-remove\" style=\"float: right; color: red;\"></i>' +
        '<i id=\"' + ability._id + '\" class=\"upAbility glyphicon glyphicon-arrow-up\" style=\"float: right; color: black;\"></i>' +
        '<i id=\"' + ability._id + '\" class=\"downAbility glyphicon glyphicon-arrow-down\" style=\"float: right; color: black;\"></i>' +
        '</div>';
      ability['html'] = abilityHtml;
    }
  }
  this.abilities = new ReactiveVar(abilities);
  // $("#keywords").pickList();
})
Template.newUnitModal.helpers({
  attackType: function() {
    var selectedAttack = Template.instance().selectedAttack.get();
    if (selectedAttack != undefined) {
      return selectedAttack['type'];
    }
  },
  attackAttribute: function(key) {
    var selectedAttack = Template.instance().selectedAttack.get();
    if (selectedAttack != undefined) {
      console.log(key);
      console.log(selectedAttack);
      return selectedAttack[key];
    }
  },
  abilityAttribute: function(key) {
    var selectedAbility = Template.instance().selectedAbility.get();
    if (selectedAbility != undefined) {
      return selectedAbility[key];
    }
  },
  keywords: function() {
    return Template.instance().keywords.get();
  },
  abilities: function() {
    return Template.instance().abilities.get();
  },
  attacks: function() {
    return Template.instance().attacks.get();
  },
  selectOption: function(value) {
    var option = Template.instance().selectedAttack.get()
    if (option != undefined && option.type == value) {
      return true
    }
    return false;
  },
  selectType: function(value) {
    var option = Template.instance().selectedAbility.get()
    if (option != undefined && option.type == value) {
      return true
    }
    return false;
  }
})
Template.newUnitModal.events({
  'click #addRow': function(event) {
    $("#damageTable tbody").append($("#damageTable tr:first").clone());
  },
  'click #addColumn': function(event) {
    $("#damageTable tr").append('<td style="text-align: center;"><input type="text" class="form-control pull-left"></td>');
  },
  'click .upAttack': function(event) {
    var attacks = Template.instance().attacks.get();
    for (index = 0; index < attacks.length; index++) {
      var attack = attacks[index];
      if (attack._id == this._id) {
        break;
      }
    }
    if (index != 0 && index != attacks.length) {
      var index2 = index--;
      var temp = attacks[index];
      attacks[index] = attacks[index2];
      attacks[index2] = temp;
    }
    Template.instance().attacks.set(attacks);
  },
  'click .downAttack': function(event) {
    var attacks = Template.instance().attacks.get();
    for (index = 0; index < attacks.length; index++) {
      var attack = attacks[index];
      if (attack._id == this._id) {
        break;
      }
    }
    if (index != attacks.length - 1 && index != attacks.length) {
      var index2 = index++;
      var temp = attacks[index];
      attacks[index] = attacks[index2];
      attacks[index2] = temp;
    }
    Template.instance().attacks.set(attacks);
  },
  'click .upAbility': function(event) {
    var abilities = Template.instance().abilities.get();
    for (index = 0; index < abilities.length; index++) {
      var ability = abilities[index];
      if (ability._id == this._id) {
        break;
      }
    }
    if (index != 0 && index != abilities.length) {
      var index2 = index--;
      var temp = abilities[index];
      abilities[index] = abilities[index2];
      abilities[index2] = temp;
    }
    Template.instance().abilities.set(abilities);
  },
  'click .downAbility': function(event) {
    var abilities = Template.instance().abilities.get();
    for (index = 0; index < abilities.length; index++) {
      var ability = abilities[index];
      if (ability._id == this._id) {
        break;
      }
    }
    if (index != abilities.length - 1 && index != abilities.length) {
      var index2 = index++;
      var temp = abilities[index];
      abilities[index] = abilities[index2];
      abilities[index2] = temp;
    }
    Template.instance().abilities.set(abilities);
  },
  'click .attackRow': function(event) {
    if (!$(event.target).hasClass('upAttack') && !$(event.target).hasClass('downAttack')) {
      Template.instance().selectedAttack.set(this);
    }
  },
  'click .abilityRow': function(event) {
    if (!$(event.target).hasClass('upAbility') && !$(event.target).hasClass('downAbility')) {
      var ability = this;
      var phases = ability.phase;
      for (index = 0; index < phases.length; index++) {
        var phase = phases[index];
        $('.abilityPhase').each(function() {
          if ($(this).val() == phase) {
            $(this).addClass('btn-warning');
          } else {
            $(this).removeClass('btn-warning');
          }
        });
      }

      Template.instance().selectedAbility.set(this);
    }
  },
  'click #addKeyword': function() {
    var keyword = $('#keywords').val().toUpperCase();
    if (keyword != '') {
      var keywords = Template.instance().keywords.get();
      if (keyword.indexOf(',') > 0) {
        console.log(keyword.split(', '))
        Template.instance().keywords.set(keywords.concat(keyword.split(', ')));
      } else {
        // keyword += '<i id=\"'+keyword+'\" class=\"deleteKeyword glyphicon glyphicon-remove\" style=\"float: right; color: red;\"></i>';
        keywords.push(keyword);
        Template.instance().keywords.set(keywords);
      }
    }
    $('#keywords').val('');
  },
  'click .deleteKeyword': function() {
    var id = $(event.target).attr('id');

    var keywords = Template.instance().keywords.get();
    for (var index = 0; index < keywords.length; index++) {
      var keyword = keywords[index];
      if (keyword.indexOf(id) > -1) {
        keywords.splice(index, 1);
      }
    }
    Template.instance().keywords.set(keywords);
    $(event.target).parent().remove()
  },
  'click #addAttack': function() {
    var attackName = $('#attackName').val();
    var attackType = $('#attackType').val();
    var attackRange = $('#attackRange').val()
    var attackNum = $('#attackNum').val();
    var attackHit = $('#attackHit').val();
    var attackWound = $('#attackWound').val();
    var attackRend = (($('#attackRend').val() == '') ? '-' : $('#attackRend').val());
    var attackDamage = $('#attackDamage').val();
    var averageWounds = 0;

    Template.instance().selectedAttack.set();

    if (attackName != '' && attackType != '') {
      var averageWounds = checkRanges(attackNum, attackHit, attackWound, attackDamage, attackRend);
      var attackObj = {
        name: attackName,
        type: attackType,
        range: attackRange,
        numAttacks: attackNum,
        toHit: attackHit,
        toWound: attackWound,
        rend: attackRend,
        damage: attackDamage,
        averageWounds: averageWounds
      }

      var attackHtml = '<td>' + attackName + '</td><td>' + attackType + '</td><td>' + attackRange + '</td><td>' + attackNum + '</td><td>' + attackHit + '</td><td>' + attackWound + '</td><td>' + attackRend + '</td><td>' + attackDamage + '</td><td>' + averageWounds + '</td>' +
        '<td>' +
        '<i id=\"' + attackObj._id + '\" class=\"deleteAttack glyphicon glyphicon-remove\" style=\"float: right; color: red; display:inline;\"></i>' +
        '<i id=\"' + attackObj._id + '\" class=\"upAttack glyphicon glyphicon-arrow-up\" style=\"float: right; color: black; display:inline;\"></i>' +
        '<i id=\"' + attackObj._id + '\" class=\"downAttack glyphicon glyphicon-arrow-down\" style=\"float: right; color: black; display:inline;\"></i>' +
        '</td>';
      attackObj['html'] = attackHtml;

      var attacks = Template.instance().attacks.get();
      var found = false;
      for (index = 0; index < attacks.length; index++) {
        var attack = attacks[index];
        if (attack.name == attackName && attack.type == attackType) {
          attackObj['_id'] = attack._id;
          attacks[index] = attackObj;
          found = true;
          break;
        }
      }
      if (!found) {
        attackObj['_id'] = randomCharacters(9);
        attacks.push(attackObj);
      }

      $('#attackName').val('');
      $('#attackType').val('');
      $('#attackRange').val('')
      $('#attackNum').val('');
      $('#attackHit').val('');
      $('#attackWound').val('');
      $('#attackRend').val('');
      $('#attackDamage').val('');

      Template.instance().attacks.set(attacks);
    }
  },
  'click .deleteAttack': function() {
    var id = $(event.target).attr('id');

    var attacks = Template.instance().attacks.get();
    for (var index = 0; index < attacks.length; index++) {
      var attack = attacks[index];
      if (attack._id == id) {
        attacks.splice(index, 1);
      }
    }
    Template.instance().attacks.set(attacks);
    $(event.target).parent().parent().remove();
  },
  'click #addAbility': function() {
    var abilityName = $('#abilityName').val();
    var abilityType = $('#abilityType').val();
    var abilityText = $('#abilityText').val().replace(new RegExp("\n", "g"), ' ');
    Template.instance().selectedAbility.set();
    var phases = [];
    $('.abilityPhase').each(function() {
      if ($(this).hasClass('btn-warning')) {
        var text = $(this).val();
        phases.push(text);
        $(this).removeClass('btn-warning');
        $(this).addClass('btn-default');
      }
    });
    var abilityObj = {};
    abilityObj['phase'] = phases;

    if (abilityType != '' && abilityText != '') {
      var abilities = Template.instance().abilities.get();
      abilityObj['_id'] = randomCharacters(17);
      abilityObj['name'] = abilityName;
      abilityObj['type'] = abilityType;
      abilityObj['text'] = abilityText;

      var abilityHtml =
        '<div class="col-lg-3">' +
        '<strong>' + abilityName + '</strong> (' + abilityType + '):' +
        '</div>' +
        '<div style="overflow-x: auto" class="col-lg-5">' +
        abilityText +
        '</div>' +
        '<div class="col-lg-2">' +
        phases +
        '</div>' +
        '<div class="col-lg-2">' +
        '<i id=\"' + abilityObj['_id'] + '\" class=\"deleteAbility glyphicon glyphicon-remove\" style=\"float: right; color: red;\"></i>' +
        '<i id=\"' + abilityObj['_id'] + '\" class=\"upAbility glyphicon glyphicon-arrow-up\" style=\"float: right; color: black;\"></i>' +
        '<i id=\"' + abilityObj['_id'] + '\" class=\"downAbility glyphicon glyphicon-arrow-down\" style=\"float: right; color: black;\"></i>' +
        '</div>';
      abilityObj['html'] = abilityHtml;

      var abilities = Template.instance().abilities.get();
      var found = false;
      for (index = 0; index < abilities.length; index++) {
        var ability = abilities[index];
        if (ability.name == abilityName) {
          abilities[index] = abilityObj;
          found = true;
          break;
        }
      }
      if (!found) {
        abilities.push(abilityObj);
      }

      $('#abilityName').val('');
      $('#abilityType').val('');
      $('#abilityText').val('');

      Template.instance().abilities.set(abilities);
    }
  },
  'click .deleteAbility': function() {
    var id = $(event.target).attr('id');

    var abilities = Template.instance().abilities.get();
    for (var index = 0; index < abilities.length; index++) {
      var ability = abilities[index];
      if (ability._id == id) {
        abilities.splice(index, 1);
      }
    }
    Template.instance().abilities.set(abilities);
    $(event.target).parent().remove()
  },
  'click #abilityPhase': function(event) {
    if ($(event.target).hasClass('btn-default')) {
      $(event.target).removeClass('btn-default');
      $(event.target).addClass('btn-warning');
    } else {
      $(event.target).addClass('btn-default');
      $(event.target).removeClass('btn-warning');
    }
  },
  'click #saveUnit': function(event) {
    event.preventDefault();
    var attacks = Template.instance().attacks.get();
    var abilities = Template.instance().abilities.get();

    for (var index = 0; index < attacks.length; index++) {
      delete attacks[index].html;
    }
    for (var index2 = 0; index2 < abilities.length; index2++) {
      delete abilities[index2].html;
    }

    var id = (this._id == undefined ? randomCharacters(17) : this._id);
    var name = $('#name').val();
    var allegiance = $('#allegiance').val();
    var costPerUnit = $('#costPerUnit').val();
    var keywords = Template.instance().keywords.get();
    var leadership = $('#leadership').val();
    var maxModelsPerUnit = $('#maxModelsPerUnit').val();
    var minModelsPerUnit = $('#minModelsPerUnit').val();
    var maxModelsDiscount = $('#maxModelsDiscount').val();
    var modelsPerKit = $('#modelsPerKit').val();
    var move = $('#move').val();
    var price = $('#price').val();
    var role = $('#role').val();
    var save = $('#save').val();
    var wounds = $('#wounds').val();
    var canCast = $('#canCast').val();
    var canUnbind = $('#canUnbind').val();

    var array = [];
    $('#damageTable tr').has('td').each(function() {
      var arrayItem = [];
      $('td input', $(this)).each(function(index, item) {
        var item = $(item).val();
        if (item != '' && item != undefined) {
          console.log(item)
          arrayItem.push(item);
        }
      });
      if (arrayItem.length > 0)
        array.push(arrayItem);
    });

    var unit = {
      _id: id,
      allegiance: allegiance.toUpperCase(),
      name: name,
      attacks: attacks,
      costPerUnit: parseInt(costPerUnit),
      keywords: keywords,
      leadership: leadership,
      maxModelsPerUnit: maxModelsPerUnit,
      minModelsPerUnit: minModelsPerUnit,
      modelsPerKit: modelsPerKit,
      maxUnitSizeCost: maxModelsDiscount,
      move: move,
      price: price,
      save: save,
      wounds: wounds,
      role: role,
      canCast: canCast,
      canUnbind: canUnbind,
      abilities: abilities
    }
    if (array.length > 0)
      unit['damageTable'] = array;

    Meteor.call('upsertUnit', unit);
    Modal.hide();
  }
})
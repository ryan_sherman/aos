import {
  Units
} from '../Model/collections.js';
import {
  Allegiance_Abilities
} from '../Model/collections.js';
import {
  Artifacts
} from '../Model/collections.js';
import {
  Command_Traits
} from '../Model/collections.js';
import {
  Endless_Spells
} from '../Model/collections.js';
import {
  Spells
} from '../Model/collections.js';
import {
  Battalions
} from '../Model/collections.js';
import '../global/jQuery-sortable.js';
import '../global/globalClient.js';
Template.armyBuilder.onCreated(function() {
  Attacks = new Meteor.Collection(null);
  this.realm = new ReactiveVar('None');
  this.artefacts = new ReactiveVar([]);
  this.armyLeaders = new ReactiveVar([]);
  this.armyUnits = new ReactiveVar([]);
  this.armyEndlessSpells = new ReactiveVar([]);
  this.armyBattleline = new ReactiveVar([]);
  this.armyBehemoths = new ReactiveVar([]);
  this.armyWarmachines = new ReactiveVar([]);
  this.armyEndlessSpells = new ReactiveVar([]);
  this.armyBattalions = new ReactiveVar([]);
  this.armyPoints = new ReactiveVar(0);
  this.allyPoints = new ReactiveVar(0);
  this.filter = new ReactiveVar();
  this.possibleLeadersCount = new ReactiveVar(0);
  this.possibleUnitCount = new ReactiveVar(0);
  this.possibleBehemothCount = new ReactiveVar(0);
  this.possibleArtilleryCount = new ReactiveVar(0);
  this.possibleBattalionsCount = new ReactiveVar(0);
  this.possibleEndlessSpellsCount = new ReactiveVar(0);
  this.unitArtifacts = new ReactiveVar([]);
  Meteor.subscribe('endlessSpells')
  Meteor.subscribe('spells');

  this.autorun(function() {
    var units = Template.instance().armyLeaders.get();

    var behemoths = [];
    for (index = 0; index < units.length; index++) {
      var unit = units[index];
      if (unit.keywords != undefined && unit.role == 'Behemoth') {
        behemoths.push(unit);
      }
    }
    Template.instance().armyBehemoths.set(behemoths);
  });
  this.autorun(function() {
    var units = Template.instance().armyUnits.get();

    //check for behemoths
    var behemoths = [];
    for (index = 0; index < units.length; index++) {
      var unit = units[index];
      if (unit.keywords != undefined && unit.role == 'Behemoth') {
        behemoths.push(unit);
      }
    }
    Template.instance().armyBehemoths.set(behemoths);

    //check for artillery
    var artillery = [];
    for (index = 0; index < units.length; index++) {
      var unit = units[index];
      if (unit.keywords != undefined && unit.role == 'Artillery') {
        artillery.push(unit);
      }
    }
    Template.instance().armyWarmachines.set(artillery);
  });
});
Template.armyBuilder.onRendered(function() {
  $("#leadersList").sortable();
  $("#unitsList").sortable();
  $("#otherUnitsList").sortable();
});
Template.armyBuilder.events({
  'click #attackRow': function(event) {
    console.log(this)
    Modal.show('damageCalc', this);
  },
  'click .heroName': function(event) {
    Modal.show('warScroll', this);
  },
  'click .editUnit': function(event) {
    var id = $(event.target).attr('id');
    var unit = Units.findOne({
      _id: id
    });
    Modal.show('newUnitModal', unit);
  },
  'click #expandAbilities': function(e, ui) {
    $(e.target.parentElement).find("#unitAbility").slideToggle('slow');
    $(e.target.parentElement).find('#collapseAbilities').toggle();
    $(e.target.parentElement).find('#expandAbilities').toggle();
  },
  'click #collapseAbilities': function(e, ui) {
    $(e.target.parentElement).find("#unitAbility").slideToggle('slow');
    $(e.target.parentElement).find('#collapseAbilities').toggle();
    $(e.target.parentElement).find('#expandAbilities').toggle();
  },
  'change #realm': function(e, ui) {
    var realm = e.target.value;
    Template.instance().realm.set(realm);
    Meteor.subscribe('realmAllegianceAbilities', realm);
  },
  'change #artefacts': function(e, ui) {
    var artefactId = e.target.value;
    var artefact = Artifacts.findOne({
      _id: artefactId
    });
    var unit = this;
    if (artefact != undefined) {
      this['artefact'] = artefact;
      var leaders = Template.instance().armyLeaders.get();
      var unitIndex = leaders.map(function(e) {
        return e.id;
      }).indexOf(unit.id);
      leaders[unitIndex] = unit;
      Template.instance().armyLeaders.set(leaders);
    } else {
      this['artefact'] = null;
      var leaders = Template.instance().armyLeaders.get();
      var unitIndex = leaders.map(function(e) {
        return e.id;
      }).indexOf(unit.id);
      leaders[unitIndex] = unit;
      Template.instance().armyLeaders.set(leaders);
    }
  },
  'change #delusionTrait': function(e, ui) {
    var delusionId = e.target.value;
    var delusion = Command_Traits.findOne({
      _id: delusionId
    });

    if (delusion != undefined) {
      $('#delusionInfo').html(delusion.text);
    }
  },
  'change #commandTrait': function(e, ui) {
    var commandId = e.target.value;
    var command = Command_Traits.findOne({
      _id: commandId
    });

    if (command != undefined) {
      $('#commandInfo').html(command.text);
    }
  },
  'change #spells': function(e, ui) {
    var spellId = e.target.value;
    var spell = Spells.findOne({
      _id: spellId
    });
    var unit = this;
    if (spell != undefined) {
      spell.type = "Spell";
      spell.added = true;
      unit.abilities.push(spell);
      var leaders = Template.instance().armyLeaders.get();
      var unitIndex = leaders.map(function(e) {
        return e.id;
      }).indexOf(unit.id);
      leaders[unitIndex] = unit;
      Template.instance().armyLeaders.set(leaders);
    }
  },
  'change #leaders': function(e, ui) {
    var unitId = e.target.value;
    var unit = Units.findOne({
      _id: unitId
    });
    $('#leadersDefault').prop('selected', true);
    if (unit != undefined) {
      var armyLeaders = Template.instance().armyLeaders.get();
      unit.id = randomCharacters(9);
      armyLeaders.push(unit);
      Template.instance().armyLeaders.set(armyLeaders);
    }
  },
  'change #units': function(e, ui) {
    var unitId = e.target.value;
    var unit = Units.findOne({
      _id: unitId
    });
    console.log(unit)
    $('#unitsDefault').prop('selected', true);
    if (unit != undefined) {
      var armyUnits = Template.instance().armyUnits.get();
      unit.id = randomCharacters(9);
      armyUnits.push(unit);
      Template.instance().armyUnits.set(armyUnits);
    }
  },
  'change #behemoths': function(e, ui) {
    var unitId = e.target.value;
    var unit = Units.findOne({
      _id: unitId
    });
    $('#behemothsDefault').prop('selected', true);
    if (unit != undefined) {
      var armyBehemoths = Template.instance().armyUnits.get();
      unit.id = randomCharacters(9);
      armyBehemoths.push(unit);
      Template.instance().armyUnits.set(armyBehemoths);
    }
  },
  'change #battalions': function(e, ui) {
    var unitId = e.target.value;
    var unit = Battalions.findOne({
      _id: unitId
    });
    $('#battalionsDefault').prop('selected', true);
    if (unit != undefined) {
      var armyBattalions = Template.instance().armyBattalions.get();
      unit.id = randomCharacters(9);
      armyBattalions.push(unit);
      Template.instance().armyBattalions.set(armyBattalions);
    }
  },
  'change #endlessSpells': function(e, ui) {
    var spellId = e.target.value;
    var spell = Endless_Spells.findOne({
      _id: spellId
    });
    $('#endlessSpellsDefault').prop('selected', true);
    if (spell != undefined) {
      var spells = Template.instance().armyEndlessSpells.get();
      spell.id = randomCharacters(9);
      spells.push(spell);
      Template.instance().armyEndlessSpells.set(spells);
    }
  },
  'change #warmachines': function(e, ui) {
    var unitId = e.target.value;
    var unit = Units.findOne({
      _id: unitId
    });
    $('#warmachinesDefault').prop('selected', true);
    if (unit != undefined) {
      var armyWarmachines = Template.instance().armyUnits.get();
      unit.id = randomCharacters(9);
      armyWarmachines.push(unit);
      Template.instance().armyUnits.set(armyWarmachines);
    }
  },
  'click #trashSpell': function(e, ui) {
    var spellId = this._id;
    var unitId = this.unitId;
    var leadersList = Template.instance().armyLeaders.get();
    breakPoint:
      for (index = 0; index < leadersList.length; index++) {
        var leader = leadersList[index];
        if (leader._id == unitId) {
          for (index2 = 0; index2 < leader.abilities.length; index2++) {
            var ability = leader.abilities[index2];
            if (ability._id == spellId) {
              leader.abilities.splice(index2, 1);
              leadersList[index] = leader;
              Template.instance().armyLeaders.set(leadersList);
              break breakPoint;
            }
          }
        }
      }
  },
  'click #trashLeaderUnit': function() {
    var unit = this;
    var leadersList = Template.instance().armyLeaders.get();
    for (index = 0; index < leadersList.length; index++) {
      var leader = leadersList[index];
      if (leader.id == unit.id) {
        leadersList.splice(index, 1);
      }
    }
    Template.instance().armyLeaders.set(leadersList);
  },
  'click #trashUnitUnit': function() {
    var unitClicked = this;
    var unitsList = Template.instance().armyUnits.get();
    for (index = 0; index < unitsList.length; index++) {
      var unit = unitsList[index];
      if (unit.id == unitClicked.id) {
        unitsList.splice(index, 1);
      }
    }
    Template.instance().armyUnits.set(unitsList);
  },
  'click #trashBattalions': function() {
    var unitClicked = this;
    var unitsList = Template.instance().armyBattalions.get();
    for (index = 0; index < unitsList.length; index++) {
      var unit = unitsList[index];
      if (unit.id == unitClicked.id) {
        unitsList.splice(index, 1);
      }
    }
    Template.instance().armyBattalions.set(unitsList);
  },
  'click #trashEndlessSpells': function() {
    var unitClicked = this;
    var unitsList = Template.instance().armyEndlessSpells.get();
    for (index = 0; index < unitsList.length; index++) {
      var unit = unitsList[index];
      if (unit.id == unitClicked.id) {
        unitsList.splice(index, 1);
      }
    }
    Template.instance().armyEndlessSpells.set(unitsList);
  },
  'click #generalCheckbox': function() {
    var general = this;
    var checked = $('#generalCheckbox').prop("checked")

    var leadersList = Template.instance().armyLeaders.get();
    for (leadersIndex = 0; leadersIndex < leadersList.length; leadersIndex++) {
      var leader = leadersList[leadersIndex];
      if (leader._id == general._id) {
        leader.general = checked;
      } else {
        leader.general = false;
      }
    }
    Template.instance().armyLeaders.set(leadersList);
  },
  'click #plusUnit': function() {
    this.numOfUnits++;
    var units = Template.instance().armyUnits.get();
    Template.instance().armyUnits.set(units);
  },
  'click #minusUnit': function() {
    this.numOfUnits--;
    var units = Template.instance().armyUnits.get();
    Template.instance().armyUnits.set(units);
  },
  'click #copyUnit': function() {
    var _ = require('underscore');
    var units = Template.instance().armyUnits.get();
    var copy = _.clone(this);

    for (var index = 0, unitLength = units.length; index < unitLength; index++) {
      if (units[index].id == copy.id) {
        copy.id = randomCharacters(9);
        units.splice(index, 0, copy);
        break;
      }
    }
    Template.instance().armyUnits.set(units);
  },
  'click #copyLeader': function() {
    var _ = require('underscore');
    var units = Template.instance().armyLeaders.get();
    var copy = _.clone(this);
    for (var index = 0, unitLength = units.length; index < unitLength; index++) {
      if (units[index].id == copy.id) {
        copy.id = randomCharacters(9);
        units.splice(index, 0, copy);
        break;
      }
    }
    Template.instance().armyLeaders.set(units);
  },
  'change #weaponOption': function(e, ui) {
    var attackId = e.target.value;
    var attack = Attacks.findOne({
      _id: attackId
    });

    if (attack != undefined) {
      var numAttacks = attack.numAttacks;
      var toHit = attack.toHit;
      var toWound = attack.toWound;
      var damage = attack.damage;
      var rend = attack.rend;

      var returnValue = checkRanges(numAttacks, toHit, toWound, damage, rend);

      var target = e.target;
      var parent = target.parentElement;
      var row = parent.parentElement;
      var table = row.parentElement;

      var cellsTotal = row.cells.length;
      for (int = 1; int < cellsTotal; int++) {
        row.deleteCell(1);
      }

      var x = row.insertCell(-1);
      x.innerHTML = attack.type;
      x.style = "text-align:center";

      x = row.insertCell(-1);
      x.innerHTML = attack.range;
      x.style = "text-align:center";

      x = row.insertCell(-1);
      x.innerHTML = attack.numAttacks;
      x.style = "text-align:center";

      x = row.insertCell(-1);
      x.innerHTML = attack.toHit;
      x.style = "text-align:center";

      x = row.insertCell(-1);
      x.innerHTML = attack.toWound;
      x.style = "text-align:center";

      x = row.insertCell(-1);
      x.innerHTML = attack.rend;
      x.style = "text-align:center";

      x = row.insertCell(-1);
      x.innerHTML = attack.damage;
      x.style = "text-align:center";

      x = row.insertCell(-1);
      x.innerHTML = returnValue;
      x.style = "text-align:center";

      var row2 = document.getElementById("linkedRow");
      if (row2 != undefined && row2 != null) {
        table.deleteRow(row2.rowIndex);
      }

      if (attack.linkId != undefined) {
        var attacks = Attacks.find({
          linkId: attack.linkId,
          _id: {
            $ne: attack._id
          }
        }).fetch();
        for (var aIndex = 0; aIndex < attacks.length; aIndex++) {
          var attackLinked = attacks[aIndex];
          if (attackLinked != undefined) {
            var newRow = table.insertRow(row.rowIndex + 1)
            newRow.style = "height: 30px";
            newRow.setAttribute("id", "linkedRow", 0);

            if (attack.name != attackLinked.name) {
              x = newRow.insertCell(-1);
              x.innerHTML = attackLinked.name;
              x.style = "text-align:center";
            } else {
              newRow.insertCell(-1);
            }

            x = newRow.insertCell(-1);
            x.innerHTML = attackLinked.type;
            x.style = "text-align:center";

            x = newRow.insertCell(-1);
            x.innerHTML = attackLinked.range;
            x.style = "text-align:center";

            x = newRow.insertCell(-1);
            x.innerHTML = attackLinked.numAttacks;
            x.style = "text-align:center";

            x = newRow.insertCell(-1);
            x.innerHTML = attackLinked.toHit;
            x.style = "text-align:center";

            x = newRow.insertCell(-1);
            x.innerHTML = attackLinked.toWound;
            x.style = "text-align:center";

            x = newRow.insertCell(-1);
            x.innerHTML = attackLinked.rend;
            x.style = "text-align:center";

            x = newRow.insertCell(-1);
            x.innerHTML = attackLinked.damage;
            x.style = "text-align:center";

            var numAttacks = attackLinked.numAttacks;
            var toHit = attackLinked.toHit;
            var toWound = attackLinked.toWound;
            var damage = attackLinked.damage;
            var rend = attackLinked.rend;

            var returnValue = checkRanges(numAttacks, toHit, toWound, damage, rend);

            x = newRow.insertCell(-1);
            x.innerHTML = returnValue;
            x.style = "text-align:center";
          }
        }
      }
      // else{
      //   var row = document.getElementById("linkedRow");
      //   if(row != undefined){
      //     table.deleteRow(row.rowIndex);
      //   }
      // }
    }
  },
  'input #keywordFilter': function() {
    var filter = $('#keywordFilter').val();
    if (filter != undefined) {
      filter = filter.toUpperCase();
      Template.instance().filter.set(filter)
    } else {
      Template.instance().filter.set()
    }
  },
})
Template.armyBuilder.helpers({
  fleshEater: function() {
    var armyAlliegance = Session.get('allegiance');
    if (armyAlliegance == 'Flesh-Eater Courts')
      return true;
    else {
      return false;
    }
  },
  unitSpells: function(unit) {
    var spells = [];
    for (index = 0; index < unit.abilities.length; index++) {
      var ability = unit.abilities[index];
      if (ability.type == 'Spell') {
        ability['unitId'] = unit._id;
        spells.push(ability)
      }
    }

    return spells;
  },
  spellsAvailable: function(keywords) {
    var spells = Spells.find({
      lore: {
        $in: keywords
      },
      allegiance: {
        $in: keywords
      }
    }).count();
    if (spells > 0) {
      return true;
    }
    return false;
  },
  spells: function(keywords) {
    if (keywords != undefined && keywords.length > 0) {
      var spells = Spells.find({
        lore: {
          $in: keywords
        },
        allegiance: {
          $in: keywords
        }
      }).fetch();
      return spells;
    }
  },
  //how many commandPoints the army will start out with
  army_CommandPoints: function() {
    var points = Template.instance().armyPoints.get();

    var defaultCommand = 1;

    var leftovers;
    if (points <= 1000) {
      leftovers = 1000 - points;
    } else if (points <= 2000) {
      leftovers = 2000 - points;
    } else {
      leftovers = 2500 - points;
    }

    leftovers = Math.floor(leftovers / 50);
    defaultCommand += leftovers;

    return defaultCommand;
  },
  //total points cost of the army
  army_MatchedPoints: function() {
    var leaders = Template.instance().armyLeaders.get();
    var units = Template.instance().armyUnits.get();
    var spells = Template.instance().armyEndlessSpells.get();
    var battalions = Template.instance().armyBattalions.get();

    var totalPoints = 0;
    for (index = 0; index < spells.length; index++) {
      var spell = spells[index];
      totalPoints = totalPoints + parseInt(spell.costPerUnit);
    }
    for (index = 0; index < battalions.length; index++) {
      var battalion = battalions[index];
      totalPoints = totalPoints + parseInt(battalion.points);
    }
    for (index = 0; index < leaders.length; index++) {
      var leader = leaders[index];
      totalPoints = totalPoints + parseInt(leader.costPerUnit);
    }
    for (index = 0; index < units.length; index++) {
      var unit = units[index];
      if (unit.numOfUnits == undefined) {
        unit.numOfUnits = 1;
      }
      console.log(unit)
      var numOfModels = unit.numOfUnits * unit.minModelsPerUnit;
      if (numOfModels == unit.maxModelsPerUnit && unit.maxUnitSizeCost != undefined && unit.maxUnitSizeCost != '') {
        totalPoints = totalPoints + parseInt(unit.maxUnitSizeCost);
      } else {
        totalPoints = totalPoints + parseInt(unit.numOfUnits * unit.costPerUnit);
      }
    }
    Template.instance().armyPoints.set(totalPoints);
    return totalPoints;
  },
  //how many allied points the army is using
  army_AllyPoints: function() {
    var armyAlliegance = Session.get('allegiance');
    if (armyAlliegance != undefined) {
      armyAlliegance = armyAlliegance.toUpperCase();
    }
    var leaders = Template.instance().armyLeaders.get();
    var units = Template.instance().armyUnits.get();

    var totalPoints = 0;
    for (index = 0; index < leaders.length; index++) {
      var leader = leaders[index];
      if (leader.keywords != undefined && !leader.keywords.contains(armyAlliegance)) {
        totalPoints = totalPoints + parseInt(leader.costPerUnit);
      }
    }
    // for(index=0;index<units.length;index++){
    //   var unit = units[index];
    //   totalPoints = totalPoints + parseInt(unit.costPerUnit);
    // }
    Template.instance().allyPoints.set(totalPoints);

    var armyPoints = Template.instance().armyPoints.get();
    var max;
    if (totalPoints <= 1000) {
      max = '/200';
    } else if (totalPoints <= 2000) {
      max = '/400';
    } else {
      max = '/500';
    }

    return totalPoints + max;
  },
  //returns army allegiance
  army_Alliegance: function() {
    return Session.get('allegiance')
  },

  //populates the list of possible leaders the user can choose to add to their army
  possible_leaders: function() {
    var self = this;
    var filter = Template.instance().filter.get();
    var units = Units.find({
      keywords: {
        $all: ['HERO', self.toUpperCase()]
      }
    }, {
      sort: {
        costPerUnit: -1
      }
    }).fetch();
    if (filter != '' && filter != undefined) {
      units = Units.find({
        keywords: {
          $all: ['HERO', self.toUpperCase(), filter]
        }
      }).fetch();
    }
    //Remove leader from add list if they are unique and already in the army
    var leadersList = Template.instance().armyLeaders.get();
    for (leadersIndex = 0; leadersIndex < leadersList.length; leadersIndex++) {
      var leader = leadersList[leadersIndex];
      for (keywordIndex = 0; keywordIndex < leader.keywords.length; keywordIndex++) {
        var keyword = leader.keywords[keywordIndex];
        if (keyword === 'UNIQUE') {
          for (index = 0; index < units.length; index++) {
            if (leader._id == units[index]._id) {
              units.splice(index, 1);
            }
          }
        }
      }
    }

    if (units.length == 0) {
      $("#leaders").prop("disabled", true);
    } else {
      $("#leaders").prop("disabled", false);
    }
    Template.instance().possibleLeadersCount.set(units.length);
    return units;
  },
  //the number of possible leaders to choose from
  possible_LeadersCount: function() {
    return Template.instance().possibleLeadersCount.get();
  },
  //the leaders chosen to be included in the army
  army_Leaders: function() {
    return Template.instance().armyLeaders.get();
  },
  //the count of the leaders chosen
  army_LeadersCount: function() {
    var armyPoints = Template.instance().armyPoints.get();
    var leadersList = Template.instance().armyLeaders.get();

    var max;
    if (armyPoints <= 1000) {
      max = '/4';
    } else if (armyPoints <= 2000) {
      max = '/6';
    } else {
      max = '/8';
    }

    return leadersList.length + max;
  },
  //populates the list of possible units
  possible_Units: function() {
    var self = this;
    var filter = Template.instance().filter.get();
    var units = Units.find({
      $and: [{
        keywords: {
          $in: [self.toUpperCase()]
        }
      }, {
        keywords: {
          $nin: ['HERO']
        }
      }, {
        role: {
          $nin: ['Artillery', 'Behemoth']
        }
      }]
    }, {
      sort: {
        costPerUnit: 1,
        name: 1
      }
    }).fetch();
    if (filter != '' && filter != undefined) {
      units = Units.find({
        $and: [{
          keywords: {
            $all: [self.toUpperCase(), filter]
          }
        }, {
          keywords: {
            $nin: ['HERO']
          }
        }, {
          role: {
            $nin: ['Artillery', 'Behemoth']
          }
        }]
      }, {
        sort: {
          costPerUnit: 1,
          name: 1
        }
      }).fetch();
    }
    if (units.length == 0) {
      $("#units").prop("disabled", true);
    } else {
      $("#units").prop("disabled", false);
    }
    Template.instance().possibleUnitCount.set(units.length);
    return units;
  },
  //count of the possible units
  possible_UnitsCount: function() {
    return Template.instance().possibleUnitCount.get();
  },
  //army units that are not battleline
  army_OtherUnits: function() {
    var armyUnits = Template.instance().armyUnits.get();
    var armyBattleline = Template.instance().armyBattleline.get();
    var nonBattleline = [];

    for (index = 0; index < armyUnits.length; index++) {
      var unitMatch = false;
      var unit = armyUnits[index];
      for (index2 = 0; index2 < armyBattleline.length; index2++) {
        battlelineUnit = armyBattleline[index2];
        if (unit._id === battlelineUnit._id) {
          unitMatch = true;
        }
      }
      if (!unitMatch) {
        nonBattleline.push(unit);
      }
    }
    return nonBattleline;
  },
  //battleline units for the army
  army_Battleline: function() {
    var armyUnits = Template.instance().armyUnits.get();
    var armyLeaders = Template.instance().armyLeaders.get();
    var battleline = [];

    for (index = 0; index < armyUnits.length; index++) {
      var unit = armyUnits[index];

      if (unit.role != undefined) {
        if (unit.role === 'Battleline') {
          battleline.push(unit);
        } else if (unit.role.includes('Battleline - Leader')) {
          var leader = unit.role.replace('Battleline - Leader:', '');
          for (index2 = 0; index2 < armyLeaders.length; index2++) {
            var leaderUnit = armyLeaders[index2];
            if (leaderUnit.keywords.indexOf(leader.trim().toUpperCase()) >= 0 && leaderUnit.general) {
              battleline.push(unit);
            }
          }
        } else if (unit.role.includes('Battleline - Allegiance')) {
          var allegiance = unit.role.replace('Battleline - Allegiance:', '');
          var armyAllegiance = Session.get('allegiance');
          if (armyAllegiance === allegiance) {
            battleline.push(unit);
          }
        }
      }
    }

    Template.instance().armyBattleline.set(battleline);
    return battleline;
  },
  //count of the number of battleline units
  army_BattlelineCount: function() {
    var armyPoints = Template.instance().armyPoints.get();
    var armyBattleline = Template.instance().armyBattleline.get();

    var max;
    if (armyPoints <= 1000) {
      max = '/2+';
    } else if (armyPoints <= 2000) {
      max = '/3+';
    } else {
      max = '/4+';
    }

    return armyBattleline.length + max;
  },
  //populates the list of possible Artillery
  possible_Artillery: function() {
    var self = this;
    var filter = Template.instance().filter.get();
    var units = Units.find({
      $and: [{
        keywords: {
          $in: [self.toUpperCase()]
        }
      }, {
        keywords: {
          $nin: ['HERO']
        }
      }, {
        role: 'Artillery'
      }]
    }).fetch();
    if (filter != '' && filter != undefined) {
      var units = Units.find({
        $and: [{
          keywords: {
            $all: [self.toUpperCase(), filter]
          }
        }, {
          keywords: {
            $nin: ['HERO']
          }
        }, {
          role: 'Artillery'
        }]
      }).fetch();
    }

    if (units.length == 0) {
      $("#warmachines").prop("disabled", true);
    } else {
      $("#warmachines").prop("disabled", false);
    }
    Template.instance().possibleArtilleryCount.set(units.length);
    return units;
  },
  //count of all the possible artillery
  possible_ArtilleryCount: function() {
    return Template.instance().possibleArtilleryCount.get();
  },
  //count of artillery units
  army_ArtilleryCount: function() {
    var armyPoints = Template.instance().armyPoints.get();
    var armyWarmachines = Template.instance().armyWarmachines.get();

    var max;
    if (armyPoints <= 1000) {
      max = '/2';
    } else if (armyPoints <= 2000) {
      max = '/4';
    } else {
      max = '/5';
    }

    return armyWarmachines.length + max;
  },
  //populates the list of possible behemoths
  possible_Behemoths: function() {
    var self = this;
    var filter = Template.instance().filter.get();
    var units = Units.find({
      role: 'Behemoth',
      $and: [{
        keywords: {
          $all: [self.toUpperCase()]
        }
      }]
    }).fetch();
    if (filter != '' && filter != undefined) {
      var units = Units.find({
        role: 'Behemoth',
        $and: [{
          keywords: {
            $all: [self.toUpperCase(), filter]
          }
        }]
      }).fetch();
    }

    if (units.length == 0) {
      $("#behemoths").prop("disabled", true);
    } else {
      $("#behemoths").prop("disabled", false);
    }
    Template.instance().possibleBehemothCount.set(units.length);
    return units;
  },
  //count of all the possible behemoths
  possible_BehemothsCount: function() {
    return Template.instance().possibleBehemothCount.get();
  },
  //count of behemoth units
  army_BehemothCount: function() {
    var armyPoints = Template.instance().armyPoints.get();
    var armyBehemoths = Template.instance().armyBehemoths.get();

    var max;
    if (armyPoints <= 1000) {
      max = '/2';
    } else if (armyPoints <= 2000) {
      max = '/4';
    } else {
      max = '/5';
    }

    return armyBehemoths.length + max;
  },
  //creates a readable list of units for a battalion
  unitsRequiredBattalions: function() {
    var unitsRequired = this.unitsRequired;
    var strings = [];
    for (index = 0; index < unitsRequired.length; index++) {
      var unit = unitsRequired[index];
      var unitString = "";
      if (unit.name != undefined) {
        if (unit.maxNumberOfUnit == unit.minNumberOfUnit) {
          unitString = unit.maxNumberOfUnit + " ";
        } else {
          unitString = unit.minNumberOfUnit + '-' + unit.maxNumberOfUnit + ' ';
        }
        unitString += unit.name + '<br/>';
      } else {
        var unitSubset = unit.subset;
        if (unit.maxNumberOfUnit == unit.minNumberOfUnit) {
          unitString = '<strong>' + unit.maxNumberOfUnit + ' of the Following: </strong><br/>';
        } else {
          unitString = '<strong>' + unit.minNumberOfUnit + '-' + unit.maxNumberOfUnit + ' of the Following: </strong><br/>';
        }

        for (subsetIndex = 0; subsetIndex < unitSubset.length; subsetIndex++) {
          var unit2 = unitSubset[subsetIndex];
          if (unit2.name != undefined) {
            unitString += unit2.name + '<br/>';
          }
        }
      }
      strings.push(unitString);
    }
    return strings;
  },
  //army list battalions
  army_Battalions: function() {
    return Template.instance().armyBattalions.get();
  },
  //populates the list of possible battalions
  possible_Battalions: function() {
    var self = this;
    var units = Battalions.find({}).fetch();
    if (units.length == 0) {
      $("#battalions").prop("disabled", true);
    } else {
      $("#battalions").prop("disabled", false);
    }
    Template.instance().possibleBattalionsCount.set(units.length);
    return units;
  },
  //count of all the possible battalions
  possible_BattalionsCount: function() {
    return Template.instance().possibleBattalionsCount.get();
  },
  //populates the list of possible behemoths
  possible_EndlessSpells: function() {
    var self = this;
    var units = Endless_Spells.find({}).fetch();

    if (units.length == 0) {
      $("#endlessSpells").prop("disabled", true);
    } else {
      $("#endlessSpells").prop("disabled", false);
    }
    Template.instance().possibleEndlessSpellsCount.set(units.length);
    return units;
  },
  //count of all the possible Endless Spells
  possible_EndlessSpellsCount: function() {
    return Template.instance().possibleEndlessSpellsCount.get();
  },

  army_EndlessSpells: function() {
    return Template.instance().armyEndlessSpells.get();
  },
  //populates the table for the units attacks
  unitsAttacks: function() {
    var unit = this;
    for (index = 0; index < unit.attacks.length; index++) {
      var attack = unit.attacks[index]
      attack['parentId'] = unit._id;
      var exists = Attacks.findOne({
        name: attack.name,
        range: attack.range,
        numAttacks: attack.numAttacks,
        parentId: unit._id
      });
      if (exists == undefined) {
        Attacks._collection.insert(attack);
      }
    }

    var attackArray = [];
    var unOptionalAttacks = Attacks.find({
      parentId: unit._id,
      pairId: {
        $exists: 0
      },
      linkId: {
        $exists: 0
      }
    }).fetch();
    for (var index = 0; index < unOptionalAttacks.length; index++) {
      var attack = unOptionalAttacks[index];
      var numAttacks = attack.numAttacks;
      var toHit = attack.toHit;
      var toWound = attack.toWound;
      var damage = attack.damage;
      var rend = attack.rend;

      var returnValue = checkRanges(numAttacks, toHit, toWound, damage, rend);
      attackArray.push('<tr id="attackRow" style="height:30px"><td style="text-align:center">' + attack.name + '</td>' +
        '<td style="text-align:center">' + attack.type + '</td>' +
        '<td style="text-align:center">' + attack.range + '</td>' +
        '<td style="text-align:center">' + attack.numAttacks + '</td>' +
        '<td style="text-align:center">' + attack.toHit + '</td>' +
        '<td style="text-align:center">' + attack.toWound + '</td>' +
        '<td style="text-align:center">' + attack.rend + '</td>' +
        '<td style="text-align:center">' + attack.damage + '</td>' +
        '<td style="text-align:center">' + returnValue + '</td></tr>');
    }

    var optionalAttacks = Attacks.find({
      parentId: unit._id,
      pairId: {
        $exists: 1
      }
    }).fetch();
    var pairIds = [];
    for (index2 = 0; index2 < optionalAttacks.length; index2++) {
      var optionalAttack = optionalAttacks[index2];
      if (!pairIds.contains(optionalAttack.pairId)) {
        pairIds.push(optionalAttack.pairId);
      }
    }

    for (amount = 0; amount < pairIds.length; amount++) {
      var pairId = pairIds[amount];
      var optionalAttacks = Attacks.find({
        parentId: unit._id,
        pairId: pairId
      }).fetch();
      var selectedAttack;
      var dropdown =
        '<select id="weaponOption" class="btn btn-light">';
      for (var index3 = 0; index3 < optionalAttacks.length; index3++) {
        var tempAttack = optionalAttacks[index3];
        if (index3 == 0) {
          selectedAttack = optionalAttacks[index3]
          dropdown += '<option selected="true" value="' + tempAttack._id + '">' + tempAttack.name + '</option>';
        } else {
          dropdown += '<option value="' + tempAttack._id + '">' + tempAttack.name + '</option>';
        }
      }
      '</select>';

      var attack = selectedAttack;
      var numAttacks = attack.numAttacks;
      var toHit = attack.toHit;
      var toWound = attack.toWound;
      var damage = attack.damage;
      var rend = attack.rend;

      var returnValue = checkRanges(numAttacks, toHit, toWound, damage, rend);
      attackArray.push('<tr id="attackRow" style="height:30px"><td style="text-align:center">' + dropdown + '</td>' +
        '<td style="text-align:center">' + attack.type + '</td>' +
        '<td style="text-align:center">' + attack.range + '</td>' +
        '<td style="text-align:center">' + attack.numAttacks + '</td>' +
        '<td style="text-align:center">' + attack.toHit + '</td>' +
        '<td style="text-align:center">' + attack.toWound + '</td>' +
        '<td style="text-align:center">' + attack.rend + '</td>' +
        '<td style="text-align:center">' + attack.damage + '</td>' +
        '<td style="text-align:center">' + returnValue + '</td></tr>');

      if (selectedAttack != undefined && selectedAttack.linkId != undefined) {
        var attacks = Attacks.find({
          parentId: unit._id,
          linkId: selectedAttack.linkId,
          range: {
            $ne: selectedAttack.range
          }
        }).fetch();
        for (var aIndex = 0; aIndex < attacks.length; aIndex++) {
          var attack = attacks[aIndex];
          if (attack != undefined) {
            var numAttacks = selectedAttack.numAttacks;
            var toHit = selectedAttack.toHit;
            var toWound = selectedAttack.toWound;
            var damage = selectedAttack.damage;
            var rend = attack.rend;

            var returnValue = checkRanges(numAttacks, toHit, toWound, damage, rend);
            attackArray.push('<tr id="attackRow" style="height:30px"><td style="text-align:center">' + attack.name + '</td>' +
              '<td style="text-align:center">' + attack.type + '</td>' +
              '<td style="text-align:center">' + attack.range + '</td>' +
              '<td style="text-align:center">' + attack.numAttacks + '</td>' +
              '<td style="text-align:center">' + attack.toHit + '</td>' +
              '<td style="text-align:center">' + attack.toWound + '</td>' +
              '<td style="text-align:center">' + attack.rend + '</td>' +
              '<td style="text-align:center">' + attack.damage + '</td>' +
              '<td style="text-align:center">' + returnValue + '</td></tr>');
          }
        }
      }
    }
    return attackArray;
  },
  averageWounds: function(attack) {
    var averageWounds = checkRanges(attack.numAttacks, attack.toHit, attack.toWound, attack.damage, attack.rend);
    return averageWounds;
  },
  //checks if unit does not have the unique keyword
  isNotUnique: function(unit) {
    for (index = 0; index < unit.keywords.length; index++) {
      if (unit.keywords[index] == 'UNIQUE') {
        return false;
      }
    }
    return true;
  },
  //returns number of models in unit
  numModels: function(unit) {
    if (unit.numOfUnits == undefined) {
      unit.numOfUnits = 1;
    }

    return unit.numOfUnits * unit.minModelsPerUnit;
  },
  //returns calculated number of points the unit is
  pointsCost: function(unit) {
    if (unit.numOfUnits == undefined) {
      unit.numOfUnits = 1;
    }

    var numOfModels = unit.numOfUnits * unit.minModelsPerUnit;
    console.log(numOfModels);
    console.log(unit.maxUnitSizeCost);

    if (numOfModels == unit.maxModelsPerUnit && unit.maxUnitSizeCost != undefined && unit.maxUnitSizeCost != '') {
      return unit.maxUnitSizeCost;
    } else {
      return unit.numOfUnits * unit.costPerUnit;
    }
  },
  //checks if unit is at max model size
  atMax: function(unit) {
    if (unit.numOfUnits == undefined) {
      unit.numOfUnits = 1;
    }

    var numOfModels = unit.numOfUnits * unit.minModelsPerUnit;
    if (numOfModels == unit.maxModelsPerUnit) {
      return true;
    } else {
      return false;
    }
  },
  //checks if unit is at min model size
  atMin: function(unit) {
    if (unit.numOfUnits == undefined) {
      unit.numOfUnits = 1;
    }

    var numOfModels = unit.numOfUnits * unit.minModelsPerUnit;
    if (numOfModels == unit.minModelsPerUnit) {
      return true;
    } else {
      return false;
    }
  },
  //calculates the average damage for an attack
  calcDamage: function(attackStats) {
    var attack = attackStats.numAttacks;
    var toHit = attackStats.toHit;
    var toWound = attackStats.toWound;
    var damage = attackStats.damage;
    var rend = attackStats.rend;

    var returnValue = checkRanges(attack, toHit, toWound, damage, rend);
    return returnValue;
  },

  //returns a list of possible relics
  artefacts: function(keywords) {
    var realm = Template.instance().realm.get();
    if (keywords != undefined && keywords.length > 0) {
      if (realm != 'None') {
        if (realm == 'Death')
          realm = 'Shyish';
        var realmArtefacts = Artifacts.find({
          allegiance: realm,
          type: "artifacts"
        }).fetch();
        var artefacts = Artifacts.find({
          keyword: {
            $in: keywords
          },
          type: "artifacts"
        }).fetch();
        return artefacts.concat(realmArtefacts);
      } else {
        return Artifacts.find({
          keyword: {
            $in: keywords
          },
          type: "artifacts"
        }).fetch();
      }
    }
  },
  unitIsGeneral: function(unit) {
    if (unit.general) {
      return true;
    } else {
      return false;
    }
  },
  commandTrait: function(keywords) {
    if (keywords != undefined && keywords.length > 0) {
      var commands = Command_Traits.find({
        keyword: {
          $in: keywords
        },
        type: "command_trait"
      }).fetch();
      return commands;
    }
  },
  delusions: function(keywords) {
    if (keywords != undefined && keywords.length > 0) {
      var commands = Command_Traits.find({
        keyword: {
          $in: keywords
        },
        type: "delusion"
      }).fetch();
      return commands;
    }
  },
  //returns a list of all the abilities
  getAbilities: function(phase) {
    var leaders = Template.instance().armyLeaders.get();
    var units = Template.instance().armyUnits.get();
    var allUnits = units.concat(leaders);
    var spells = Template.instance().armyEndlessSpells.get();
    for (index = 0; index < spells.length; index++) {
      var spell = spells[index];
      if (spell.abilities != undefined && spell.abilities.length > 0) {
        allUnits = allUnits.concat(spell.abilities);
      }
    }
    var array = [];

    var unitsCovered = [];
    for (index = 0; index < allUnits.length; index++) {
      var unit = allUnits[index];
      if (!unitsCovered.contains(unit._id)) {
        unitsCovered.push(unit._id)

        var abilities = unit.abilities;
        if (abilities != undefined && abilities.length > 0) {
          for (abilityIndex = 0; abilityIndex < abilities.length; abilityIndex++) {
            var ability = abilities[abilityIndex];
            ability.text = ability.text.replace('\n', '<br/>');
            if (phase != 'Other') {
              if (ability.phase.contains(phase)) {
                ability.unitName = unit.name;
                array.push(ability);
              }
            } else {
              var arrayPhase = ['Persistant', 'Setup', 'Hero', 'Movement', 'Shooting', 'Charge', 'Combat', 'Battleshock']
              if (ability.phase.compareArray(arrayPhase)) {
                ability.unitName = unit.name;
                array.push(ability);
              }
            }
          }
        }
      }
    }

    var allegianceAbilities = Command_Traits.find({
      type: 'Command'
    }).fetch();
    allegianceAbilities = allegianceAbilities.concat(Allegiance_Abilities.find({
      type: 'Allegiance Ability'
    }).fetch());
    for (index2 = 0; index2 < allegianceAbilities.length; index2++) {
      var ability2 = allegianceAbilities[index2];
      if (phase != 'Other') {
        if (ability2.phase.contains(phase)) {
          ability2.unitName = "Allegiance";
          array.push(ability2);
        }
      } else {
        var arrayPhase2 = ['Persistant', 'Setup', 'Hero', 'Movement', 'Shooting', 'Charge', 'Combat', 'Battleshock'];
        if (ability2.phase.compareArray(arrayPhase2)) {
          ability2.unitName = "Allegiance";
          array.push(ability2);
        }
      }
    }
    array = array.sort(customSort);
    return array;
  },
  canCast: function(doc) {
    if (doc.canCast != undefined) {
      return "Cast " + doc.canCast + " Spell(s)";
    }
  },
  canUnbind: function(doc) {
    if (doc.canUnbind != undefined) {
      return "Unbind " + doc.canUnbind + " Spell(s)";
    }
  }
})
import { Mongo } from 'meteor/mongo';

export const Units = new Mongo.Collection("units");
export const Attacks = new Mongo.Collection("attacks");
export const Abilities = new Mongo.Collection("abilities");
export const Wargear = new Mongo.Collection("wargear");
export const Allegiances = new Mongo.Collection("allegiances");
export const Allegiance_Abilities = new Mongo.Collection("allegiance_abilities");
export const Command_Traits = new Mongo.Collection("command_traits");
export const Artifacts = new Mongo.Collection("artifacts");
export const Endless_Spells = new Mongo.Collection("endless_spells");
export const Spells = new Mongo.Collection("spells");
export const Battalions = new Mongo.Collection("battalions");
